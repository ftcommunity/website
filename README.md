# Planung, Dokumentation und Code für <https://ftcommunity.de>

Hier soll der Neubau von <https://ftcommunity.de> 
zunächst geplant und später entwickelt, gepflegt und dokumentiert werden.

<!-- Bitte diese Übersicht oben lassen! -->
## Aktueller Stand

* [ ] Planung: Was soll die neue Website können? [Details hier](doc/requirements.md)
* [ ] Planung: Wie wollen wir das umsetzen? [Details hier](doc/architecture.md)
* [ ] Umsetzung [Details hier](doc/umsetzung.md)
* [ ] Wartung und Pflege

## Ich möchte hier mitmachen - wie kann ich mich beteiligen?

Bevor ich Dir diese Frage beantworte, möchte ich zunächst ein paar Dinge vorab
hervorheben.
Du solltest:
* Konstruktiv und engagiert mitarbeiten wollen.
* Firefox oder Chrome als Browser verwenden.
  Mit dem Internet Explorer funktionieren so einige Gitlab-Dinge nicht.
* Ein wenig Englisch können.
  Die Gitlab-Oberfläche kann auf eine andere Sprache umgestellt werden.
  Angemeldete Benutzer finden das unter "Setting" => "Profile" =>
  "Main setting" => "Preferred language".
  Nachdem die gewünschte Sprache ausgewählt ist, musst Du noch ganz
  runterscrollen und "Update profile settings" anklicken.
  Aber wir nehmen hier üblicherweise die englischen Begriffe und kennen
  eventuelle deutsche Übersetzungen nicht.
* Recht gut Deutsch verstehen.
  Die Diskussionen hier werden wohl hauptsächlich in unserer Muttersprache
  stattfinden.
  Englisch geht aber sicher auch, wenn Du uns zwar verstehst, jedoch nicht
  Deutsch schreiben kannst - in den Kommentaren zu den _Commits_ und _Merge
  requests_ jedenfalls.
* Dich schon ein klein wenig mit Git auskennen oder zumindest bereit sein es
  zu lernen.
  Ganz ohne einige (englische!) Begriffe geht das nicht.
  Und für eine vollständige Anleitung wie das Git funktioniert, fehlt uns hier
  aktuell die Zeit und auch der Raum.
  Über Git gibt es Literatur, z. B.: Git, dpunkt.verlag.
  Das Buch ist aber aufgrund des Preises wohl eher nur für Dich geeignet,
  wenn Du Dich sowieso mal mit Git beschäftigen wolltest und es auch zukünftig
  nutzen möchtest.
Und nun auf ins Getümmel, die [Mitmachanleitung](doc/how_to_contribute.md) ist
wegen ihres Umfangs ausgelagert.

Oh, fast hätte ich es vergessen:
**_Danke, dass Du mithelfen möchtest_** die ftc-Seite neu und
zukunftssicher aufzubauen.
