#!/usr/bin/env/python3

# Ändert Konstrukteur und/oder Fotograf auf neuen Wert
# Felder tauchen auf in Files mit den Formaten: bild, file und wiki.
# Sollte von find ./ -name "*.md" Liste der in Frage kommenden Files kriegen.
# Aufruf mit -exec python3 editkonstrukteure.py {} FileMitAltenNamen neuerName kf \;
# k: ändert Konstrukteur, f: ändert Fotograf, kf ändert beides.

import sys
import re
import subprocess

fileName = sys.argv[1]
oldNameFileName = sys.argv[2]
newName = sys.argv[3]
params = sys.argv[4]
modified = False

# vorher: Liste der alten Namen einlesen
# wird mit allen Varianten auf einmal erledigt, um die Files nur einmal anpacken zu müssen
oldNames = []
with open(oldNameFileName,"rt") as oldNameFile:
    for line in oldNameFile:
        oldNames.append(line.rstrip())

with open(fileName, "rt") as fin:
    data = fin.read()

if re.findall(r'layout: "(wiki|file|image)"', data):
# Datei kommt prinzipiell in Frage
    lines = data.splitlines()
    datalen = len(lines)
    i = 0
    while i < datalen:
        p = re.search(r"(konstrukteure|fotografen):",lines[i])
        if p:
# Eintrag gefunden
            if p.group()[0] in params:
# Erster Buchstabe des Eintrages ist in Liste der Parameter
# Ersetzung ist gewünscht
# Namen des Konstrukteurs/Fotografen aus String herausfiltern
                x = re.search(r'- "(.*)"', lines[i+1]).group(1)
# Prüfen ob zu ersetzender alter Name vorliegt
                if x in oldNames:
# wenn ja, genau den alten Namen durch den neuen Namen ersetzende
                    lines[i+1] = lines[i+1].replace(x,newName)
                    modified = True
# weiterzählen um 2 wenn konstrukteure oder fotografen
            i = i + 2
        else:
# sonst um 1
# normale Zeile
            i = i + 1

# nur wenn Ersetzung vorgenommen wurde: Datei abspeichern
    if modified:
        with open(fileName,'w') as fout:
            for l in lines:
                print(l)
                fout.write(l)
                fout.write("\n")


