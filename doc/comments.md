# Optionen für Kommentarfunktion

(https://gohugo.io/content-management/comments/)

Kommentare sollen für alle Beiträge im Bilderpool möglich sein.
Die Kommentarfunktion und die Benutzerverwaltung werden auf dem ftc-Server gehostet.
Das Kommentarsystem muss kostenlos und OpenSource sein (FOSS).


## Disqus

Disqus ist ein Online-Dienst, der eine zentralisierte Diskussionsplattform für Websites anbietet. 

** abgelehnt ** 

## Static Man

"Staticman is a Node.js application that receives user-generated content and uploads it as data files to a GitHub repository. In practice, this allows you to have dynamic content (e.g. blog post comments) as part of a fully static website, as long as your site automatically deploys on every push to GitHub."

** abgelehnt wegen Anbindung an GitHub **

## Talkyard (Open source, & serverless hosting)

[Link](https://www.talkyard.io/)

You can install Talkyard on your own server; it's free and open source.
You need an Ubuntu 18.04 server, on which you can install Docker. 

** abgelehnt ** 

## txtpen

** abgelehnt ** (nicht aktuell)

## IntenseDebate

** abgelehnt ** (viel Marketing-bla-bla, gehostet, nicht aktuell, Wordpress zugehörig) 

## Graph Comment

** abgelehnt ** (viel Marketing-bla-bla, gehostet)

## Muut

** abgelehnt: kostenpflichtig ** 

## isso (Self-hosted, Python)

[Link](https://posativ.org/isso/) 
[Isso und Hugo](https://stiobhart.net/2017-02-24-isso-comments/)

TODO
      
## Utterances (Open source, Github comments widget built on Github issues)

"A lightweight comments widget built on GitHub issues. Use GitHub issues for blog comments, wiki pages and more!"

** abgelehnt wegen Anbindung an GitHub **

## Remark (Open source, Golang, Easy to run docker)

(https://github.com/umputun/remark)

** zurückgestellt ** braucht OAuth2-Provider
