# Umsetzung von <https://ftcommunity.de>

## Umsetzung mit Hugo

Die Umsetzung erfolgt nach [Architektur-Vorschlag D](doc/architecture.md).
Dabei macht [Hugo](https://gohugo.io/) aus Markdown fertige, direkt auslieferbare HTML-Seiten.
Als Versionsmanagement-Tool verwenden wir Gitlab.

Die Implementierung der eigentlichen Webseite mit allen Inhalten befindet sich hier:
<https://gitlab.com/ftcommunity/website-hugo-poc>
Die Skripte und sonstige Technik sind als Theme in ein eigenenes Repository ausgelagert:
<https://gitlab.com/ftcommunity/website-layout>.
Die auf unserer Webseite eingesetzte Technik ist [hier](https://ftcommunity.de/fans/techdoc/)

Für Testzwecke gibt es noch eine Fassung mit nur wenigen Inhalten hier:
<https://gitlab.com/ftcommunity/testsite>.
Dort ist auch erläutert, wie man damit arbeitet.

## Import-Skripte
Zum Import aller Inhalte aus der alten [Webseite](old.ftcommunity.de) 
und der damals verwendeten Datenbank
dienten mehrere Importskripte:

* [import-advent.py](https://gitlab.com/ftcommunity/website/-/blob/master/doc/import-advent.py) für einen alten Adventskalender
* [import-files.py](https://gitlab.com/ftcommunity/website/-/blob/master/doc/import-files.py) für die Dateien im Download-Bereich
* [import-images.py](https://gitlab.com/ftcommunity/website/-/blob/master/doc/import-images.py) für alle Bilder aus dem Bilderpool
* [neuimport-images.py](https://gitlab.com/ftcommunity/website/-/blob/master/doc/neuimport-images.py) für eine Korrektur der Daten zum Bilderpool

Außerdem gibt es noch zwei Skripte, um neue Daten halbautomatisch in die Bildersammlung hochzuladen:

* [readftpub.py](https://gitlab.com/ftcommunity/website/-/blob/master/doc/readftpub.py) zum Auslesen einer Publisher-Datei
* [upload-from-csv.py](https://gitlab.com/ftcommunity/website/-/blob/master/doc/upload-from-csv.py) zum Auslesen aus einem Verzeichnis mit csv-Datei

## Begründung für die gewählte Umsetzung

Abschließend noch einige Gründe, warum wir diese Architektur gewählt haben:

* Sicherheit: durch die Trennung der HTML-Seitenerzeugung von Webserver und Berechtigungsmanagement
* Versionierbarkeit: durch die Verwendung von Layout-Files und von Markdown-Files für die Inhalte, Einsatz von Git und Gitlab
* Verteilte Entwicklung: durch den Einsatz von Gitlab
* Skriptbarkeit: Auslesen aus der Inhalte aus der alten Datenbank bzw. vom alten Server und Schreiben in Hugos content-Verzeichnis durch Skripte


  
