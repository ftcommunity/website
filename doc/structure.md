# Vorschlag für eine Menü-Struktur
Ein Vorschlag, wie man die Seite gliedern könnte, 
unabhängig von der technischen Umsetzung.

* Bilderpool
* Forum 
* ft:pedia
* Teile-Datenbank (Link nach extern)
* Knowhow
    * Elektronik und Computing (ggf. Link nach extern)
    * Archiv
    * Downloads
    * Dokumente (aus dem Wiki)
    * Links
* Fans
    * Verein
    * Chat
    * Veranstaltungen
    
## Bilderpool
Der Vorschlag sieht 4 Kategorien vor. 
Unterhalb dieser Kategorien könnte die Einteilung durch Tags erfolgen
(dynamisch, keine hierarchische Struktur).

* Modelle 
* Veranstaltungen (Fotos von Veranstaltungen)
* Basteleien (Schnitzereien, 3D-Druck usw,)
* Dies und das (Sonstiges, Diverses, der ganze Rest)