# URLs bei ftcommunity.de

## Geschichte der Webseite Stand September 2019
Die alte dynamische Seite (kurz "adS") wurde mit dem Gallerie-System 4images erstellt.
Dieses baut die einzelnen Seiten durch PHP mit einer mySQL-Datenbank auf.

Nach einem Hackerangriff, der im November 2018 bekannt wurde,
wurde die bisherige dynamische Seite abgeschaltet und durch eine statische Kopie ersetzt
(im folgenden nasS  "noch aktuelle statische Seite"  genannt).
In der nasS stehen alle Seiten noch zur Verfügung,
aber ein Schreiben ist für Benutzer nicht mehr möglich,
und die URLs der adS geben in den meisten Fällen einen 404-Fehler.

## Aufbau der URLs der dynamischen Website
In der adS wurden die Seiten durch den Aufruf eines PHP-Skriptes mit Parametern generiert.
Dementsprechend bestand die URL aus dem Skript-Namen und den Parametern (nach dem Fragezeichen):
Beispiel: `https://www.ftcommunity.de/wiki.php?action=show&topic_id=13`

Die Kategorien im Bilderpool sahen dann z.B. so aus: `https://ftcommunity.de/categories.php?cat_id=3510`, 
die Einzelbilder so: `https://www.ftcommunity.de/details.php?image_id=34614`

Die Dateien zum Download lagen in einem Verzeichnis im Filesystem des Servers, so dass sich eine statischer URL ergibt, 
z.B. <https://www.ftcommunity.de/data/downloads/ebausteine/schaltplne/flipflop.pdf>.
Die Übersichtsseite der ft:pedia war unter <https://ftcommunity.de/ftpedia> erreichbar.
Die Einzelhefte liegen unter z.B. <https://ftpedia-ausgaben/ftpedia-2017-3.pdf>, 
da sie ebenfalls auf dem Filesystem des Server abgelegt sind.

Die ft:pedia-Übersichtsseite hatte außerdem die URL `ftcommunity.de/ftcomm.php?file=ftpedia`.

## Aufbau der URLs in der statischen Website
In der nasS hat jede Seite, die vorher mit einer dynamischen URL erreichbar war,
eine statische URL mit einem eindeutigen Dateinamen, 
sowie außerdem einem nicht mehr benötigten Teil mit dem Parameter aus der alten dynamischen URL.
Beispiel:
`https://www.ftcommunity.de/categories053c.html?cat_id=463`. 
Dabei ist `categories053c.html` der eindeutige Dateiname, bestehend aus dem entsprechenden Skriptnamen der adS und einem fortlaufenden Identifier, 
und der Teil nach dem Fragezeichen der Parameter für die frühere dynamische Abfrage.

Die Download-Dateien, die schon in der adS statische URLs hatten, 
sind weiterhin unter diesen erreichbar. 
Dies gilt auch für die Übersichtsseite der ft:pedia und die Einzelhefte.

Auf jeder Seite steht als vorletzte Zeile, direkt vor `</html>` ein Kommentar, 
aus dem die URL der entsprechenden adS-Seite hervorgeht:
`<!-- Mirrored from ftcommunity.de/categories.php?cat_id=3423 by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Nov 2018 07:53:50 GMT -->`.
