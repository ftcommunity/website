# Umsetzung der Architektur-Variante A mit Drupal

## Vorteile von Drupal
* Upload und Anzeige von Bildern Bestandteil des Core-Systems.
* Wird regelmäßig gewartet.
* Gut geeignet fü den Umgang mit Taxonomien,
z.B. Kategorien, Tags usw.
Umsetzung evtl. mit zusätzlichen Modulen.
* Module für komplexe Suche (Faceted Search) verfügbar.
* Differenzierte Berechtigungen für verschiedene Usergruppen,
z.B. nur Hochladen von Bildern für Fans, Texte für Redakteure usw.

## Nachteile
* Drupal ist ein komplexes System, dessen Konfiguration nicht trivial ist.
Die Umsetzung der Anforderungen wird gelegentlich Kompromisse erfordern,
um mit endlichem Aufwand zu Lösungen zu kommen.

## Sicherheitsaspekte

### Potentielle Sicherheitslücken
1. Betriebssystem (Linux)
1. Webserver (Apache)
1. Programmiersprache (PHP)
1. Datenbank (MariaDB)
1. CMS und Module (Drupal)

### Lösungsstrategien
Die ersten vier genannten Ebenen werden durch Updates 
durch das Paketmanagement von Linux abgedeckt.

Für Drupal-Core gab es in 2018 sechs Security Advisories, davon zwei highly critical.
Das dann unbedingt erforderliche Update dauert maximal eine Stunde, wenn keine Probleme auftreten. 
Das Update muss möglichst zeitnah (idealerweise am gleichen Tag) erfolgen.

Die erforderlichen Updates für die Module hängen von der Anzahl und der Art der Module ab.
Im Normalfall ist monatlich mit einem Update zu rechnen,
das dann ebenfalls maximal eine Stunde dauert und möglichst am gleichen Tag erfolgen soll.