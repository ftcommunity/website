# Systemarchitektur für <https://ftcommunity.de>

Für alle Lösungen wird Open-Source-Software genutzt.

## Lösung A: Verwendung eines dynamischen Content-Management-Systems

Ein _dynamisches_ oder _klassisches Content-Management-System (CMS)_ ist ein Programm,
das auf dem Server läuft und dort
dynamisch Webseiten generiert,
die dann zum Beispiel von den Rechten des angemeldeten Benutzers abhängen.
Die Daten dafür sind in einer Datenbank abgelegt.
Beispiel: [Drupal](doc/architecture_A.md), in der Programmiersprache PHP geschrieben.

### Vorteile
* Ein CMS kann wahrscheinlich alle Anforderungen (Benutzerverwaltung, Rechteverwaltung, Bilderpool) umsetzen.
* Die Absicherung erfolgt durch Sicherheitsupdates, die von den Entwicklern des CMS bereitgestellt werden.
* Die Anpassung an eigene Vorstellungen erfolgt durch die Konfiguration diverser Parameter.

### Nachteile
* Die Installation und Einrichtung ist abhängig von der Mächtigkeit des CMS nicht trivial.
* Ein großes CMS ist ein komplexes Programm mit einer unübersichtlichen Architektur.
* Durch die dynamische Seitengenerierung, den Einsatz von PHP und einer Datenbank 
  ergeben sich viele Möglichkeiten für Sicherheitslücken.
* Für die Sicherheitsupdates ist man auf die Entwickler-Community des CMS angewiesen.
* Durch die Speicherung von Inhalten und Einstellungen gemeinsam in einer Datenbank ist es schwierig,
  mehrere Entwickler parallel arbeiten zu lassen.

 
## Lösung B: Komplette eigene Programmierung mit einem Webframework (hier: Ruby on Rails)

### Vorteile
* Man ist nicht so festgelegt wie bei einem CMS
* Anforderungen können schnell implementiert werden
* Klare Trennung von Logik und Darstellung durch Model-View-Controller Architektur
* Konvention vor Konfiguration bedeutet schnelle Einarbeitung durch neue Entwickler, hohe Wartbarkeit und übersichtliche Architektur auch bei großen Projekten. Es ist wenig Dokumentation erforderlich.
* Sicherheitslücken werden durch Konventionen des Frameworks und langjährige Reifung der verwendeten Module vermieden.
* Versorgung von Sicherheitsupdates durch das Framework und verwendeten Module
* Integration von Unit- und Integrationstests Bestandteil der Architektur

### Nachteile
* Updates auf neue Major-Versionen sehr aufwendig, siehe z.B. [github](https://githubengineering.com/upgrading-github-from-rails-3-2-to-5-2/)
* Installation und Konfiguration nicht trivial, hier ist Unterstützung durch einen Entwickler erforderlich. Der sollte zwangsläufig vorhanden sein.
* Einzelne verwendete Module (gems) werden u.U. im Laufe der Zeit von den jeweiligen Entwicklern nicht mehr unterstützt. Hier sind dann umfangreiche Umbaumaßnahmen erforderlich
* Derzeit nicht auf Windows-Servern lauffähig
* Langsame Interpretersprache
* Kenntnisse in mehreren Programmiersprachen (Ruby, JavaScript) erforderlich
* Nur Backend, zusätzliche JavaScript-Frameworks wie z.B. jQuery und Vue.js im Frontend erforderlich

### Mögliche Module
* [Upload von Dateien: carrierwave](https://github.com/carrierwaveuploader/carrierwave)
* [Thumpnail Generation: minimagick](https://github.com/minimagick/minimagick)
* [Benutzerverwaltung: devise](https://github.com/plataformatec/devise)
* [CSS Framework Bootstrap](https://github.com/twbs/bootstrap-sass)

## Lösung C: Komplette eigene Programmierung mit einem modernem Webframework z.B AngularJs, React oder Vuejs

### Vorteile
* Modernes Framework
* Vollständig im Frontend, Backend stellt nur die Datenbank zur Verfügung

### Nachteile
* Viel Arbeit
* Erfordert tiefergehende und spezielle Programmierkenntnisse bei den Entwicklern

## Lösung D: Baukastensystem aus Standardkomponenten

Der Baukasten enthält die folgenden Teile:
* Eine Komponente für die Speicherung und Verwaltung der eigentlichen Inhalte.
  Das muss nicht unbedingt so wie z.B. beim klassischen CMS eine Datenbank sein,
  sondern kann auch ein Versionsverwaltungssystem 
  wie z.B. [Git](https://git-scm.com/), [Mercurial](https://www.mercurial-scm.org/) oder [Subversion](https://subversion.apache.org/) sein.
  Die (Text-)Inhalte selbst werden bevorzugt in einer einfach editierbaren 
  und einfach in andere Formate umwandelbaren Form gespeichert,
  wie z.B. [Markdown](https://commonmark.org/)
* Eine Komponente, die aus Markdown fertige, direkt auslieferbare HTML-Seiten macht.
  Beispiele: [Hugo](https://gohugo.io/), [Jekyll](https://jekyllrb.com/) und [viele andere](https://www.staticgen.com/)
* Eine Komponente, die die fertigen Seiten ausliefert.
  Hier geht eigentlich jeder Webserver. 
  Natürlich auch [Nginx](https://nginx.org/), [Apache](https://httpd.apache.org/) und die ganzen anderen bekannten Namen.
* Eine Komponente, die in die ausgelieferten statischen Seiten die gewünschte Dynamik bringt
  (wie z.B. die zufällig aus dem Bilderpool gezogenen Bilder auf der Startseite).
  Diese Komponente läuft im Browser des Benutzer 
  und wird in [JavaScript](https://de.wikipedia.org/wiki/JavaScript) 
  oder einer Variante davon wie [TypeScript](https://www.typescriptlang.org/) umgesetzt.
  Zusätzlich kann man sich auf fertige Bibliotheken und Frameworks wie bei Lösung C genannt stützen.
* Eine Komponente, mit der man die Inhalte der Website verändern kann.
  Hier gibt es eine immense Auswahl, 
  angefangen von Git und einem beliebigen Texteditor auf dem lokalen PC eines "Redakteurs",
  über Git-Weboberflächen wie [Gitea](https://gitea.io/en-us/) oder sogar [Gitlab](https://about.gitlab.com/install/)
  (ja, das kann man auf dem eigenen Server installieren),
  bis hin zu fertigen CMS-Oberflächen wie [Netlify](https://www.netlifycms.org/).
  Und natürlich ebenfalls die Option,
  selbst eine passende Weboberfläche maßzuschneidern.
* Und last not least eine Komponente,
  die sich darum kümmert den Zugang zu den anderen Komponenten zu regeln.
  Auch hier gibt mehre Möglichkeiten, von fertigen Lösungen wie z.B. [Keycloak](https://www.keycloak.org/)
  über Baukastensysteme wie das von [Ory](https://github.com/ory),
  die die Zugangskontrolle selbst nochmal in einzelne Komponenten aufteilen,
  bis hin zum Selbstbau (wobei Selbstbau gerade hier aus Sicherheitsgründen eher keine gute Idee ist).

Die meisten Komponenten dieses Baukastens sind entweder von vornherein darauf ausgerichtet,
mit dem Rest der Welt nur über eine vergleichsweise schmale Schnittstelle zu reden,
die zudem noch entweder tatsächlich standardisiert ist 
(wie z.B. [Oauth 2](https://oauth.net/2/) und [Openid Connect](https://openid.net/connect/) für den "Türsteher"),
oder zumindestens weit verbreitet (wie z.B. Git, oder Markdown als Datenformat für Inhalte).

Zu einem System wird das Ganze durch Konfigurationseinstellungen der Komponenten 
und durch "Glue-Code" — 
das sind kleine Scripte und Progrämmchen, 
die die einzelnen Komponenten miteinander verknüpfen.

### Vorteile
* Viele der Komponenten sind als Standard-Pakete in den meisten Linux-Distributionen enthalten,
  und können daher einfach aktuell gehalten werden
* Die einfachen Schnittstellen zwischen den Komponenten erlauben es,
  bei Bedarf einzelne Teile des Systems auszutauschen zu können.
* Durch den modularen Aufbau kann man das System gut zwischen Sicherheit und
  Bequemlichkeit ausbalancieren.
  Man kann zum Beispiel alle Komponenten auf einem Server und mit demselben 
  Benutzeraccount laufen lassen (bequem für den Administrator, aber nicht 
  sicherer als z.B. ein klassisches CMS), 
  oder jede Komponente auf einen eigenen, abgeschotteten Server auslagern
  (sehr sicher, bedeutet aber mehr Aufwand für den Administrator).
  Oder natürlich irgendetwas dazwischen.

### Nachteile
* So ein System ist nicht trivial aufzusetzen, hier muss man am Anfang einiges an Aufwand treiben.
* Die selbstenwickelten Teile des Systems (Konfiguration, Glue-Code, Templates für den Site-Generator,
  CSS und JavaScript-Code für die Webfrontends, ...) müssen nicht nur entwickelt sondern
  auch dauerhaft gepflegt werden.
* Wenn man sich ungeschickt anstellt, steht man am Ende statt mit einem gut wartbaren, 
  sicheren System mit einem unwartbaren Script-Verhau da, den keiner mehr durchschaut.

# Einige Schaubilder
So als Vorschläge zur weiteren Diskission, wenn die Anforderungen mal da sind.

## Übersichtsbild - Bereiche aus Benutzersicht
<!-- https://mermaidjs.github.io/flowchart.html -->
```mermaid
graph TD;
  subgraph ftc- Webseite
    %% Hier kommt die Übersicht der Bereiche
    Startseite[https://www.ftcommunity.de]---Forum
    Startseite---Downloads
    Startseite---Bilderpool
    Startseite---Chat
    Startseite---ft-pedia["ft:pedia"]
    Startseite---wiki
    %% Könnt sein, dass die Klickerei nicht geht, aber das liegt dann am Gitlab.
    click Startseite "https://www.ftcommunity.de"
    click Bilderpool "https://www.ftcommunity.de/bilderpool.html"
    click Downloads "https://www.ftcommunity.de/downloads.html"
    click Forum "https://forum.ftcommunity.de"
    click Chat "https://www.ftcommunity.de/ftcomm8cb8.html?file=chat"
    click ft-pedia "http://www.ftcommunity.de/ftpedia"
    click wiki "https://www.ftcommunity.de/wiki.html"
  end
  subgraph Assoziierte Webseiten
    Startseite---ftdb[ft Datenbank]
    click ftdb "https://ft-datenbank.de"
  end
  subgraph 3rd party
    Bilderpool---|Picture Upload|ft-pub[ft-Publisher]
  end
  subgraph 3rd party
    Chat---irc[IRC Client]
  end
```
Basiert auf der gegenwärtigen Struktur (und hat einige Ecken unterschlagen).

## Übersichtsbild - Mit Login-Server (Benutzersicht)
```mermaid
graph TD;
  subgraph ftc- Webseite
    %% Hier kommt die Übersicht der Bereiche
    Startseite[https://www.ftcommunity.de]---Forum
    Startseite---Downloads
    Startseite---Bilderpool
    Startseite---Chat
    Startseite---ft-pedia["ft:pedia"]
    Startseite---wiki
    Bilderpool---UserLogin(Nutzeridentität)
    Downloads---UserLogin
    wiki---UserLogin
    Forum---UserLogin
    %% Könnt sein, dass die Klickerei nicht geht, aber das liegt dann am Gitlab.
    click Startseite "https://www.ftcommunity.de"
    click Bilderpool "https://www.ftcommunity.de/bilderpool.html"
    click Downloads "https://www.ftcommunity.de/downloads.html"
    click Forum "https://forum.ftcommunity.de"
    click Chat "https://www.ftcommunity.de/ftcomm8cb8.html?file=chat"
    click ft-pedia "http://www.ftcommunity.de/ftpedia"
    click wiki "https://www.ftcommunity.de/wiki.html"
  end
  subgraph Assoziierte Webseiten
    Startseite---ftdb[ft Datenbank]
    click ftdb "https://ft-datenbank.de"
  end
  subgraph 3rd party
    Bilderpool---|Picture Upload|ft-pub[ft-Publisher]
  end
  subgraph 3rd party
    Chat---irc[IRC Client]
  end
```
_(Sorry für den Vehau. Die Anordnung wird vom mermaid gesteuert und die hiesige
Version ist wohl nicht die Neueste. Bestimmte Sachen die in der Doku stehen
kennt die Gitlab-Marina noch nicht. Wahrscheinlich hält Zig sie wieder vom
Fortbildungskurs ab._)

## Was sich ein Admin wünschen dürfte
```mermaid
graph TD;
  subgraph ftc- Webseite
    %% Hier kommt die Übersicht der Bereiche
    Startseite[https://www.ftcommunity.de]---Forum
    Startseite---Bilderpool
    Startseite---Downloads
    Startseite---Chat
    Startseite---ft-pedia["ft:pedia"]
    Startseite---wiki
    ft-pedia---admin[Fernwartungszugang]
    Forum---admin
    Bilderpool---admin
    Downloads---admin
    Startseite---admin
    wiki---admin
    Chat---admin
    UserLogin(Nutzeridentität)---admin
    Übungsbereich(Testserver)---admin
    %% Könnt sein, dass die Klickerei nicht geht, aber das liegt dann am Gitlab.
    click Startseite "https://www.ftcommunity.de"
    click Bilderpool "https://www.ftcommunity.de/bilderpool.html"
    click Downloads "https://www.ftcommunity.de/downloads.html"
    click Forum "https://forum.ftcommunity.de"
    click Chat "https://www.ftcommunity.de/ftcomm8cb8.html?file=chat"
    click ft-pedia "http://www.ftcommunity.de/ftpedia"
    click wiki "https://www.ftcommunity.de/wiki.html"
  end
```
## Was aus Sicherheitsaspekten als Kompromiss möglich wäre
```mermaid
graph TD;
  subgraph ftc- Webseite
    %% Hier kommt die Übersicht der Bereiche
    Startseite[https://www.ftcommunity.de]---Forum
    Startseite---Bilderpool
    Startseite---Downloads
    Startseite---Chat
    Startseite---ft-pedia["ft:pedia"]
    Startseite---wiki
    ft-pedia---admin2[Fernwartungszugang]
    Forum---admin1[Fernwartungszugang]
    Bilderpool---admin2[Fernwartungszugang]
    Downloads---admin2
    Startseite---admin2
    wiki---admin2
    Chat---admin3[Fernwartungszugang]
    UserLogin(Nutzeridentität)---admin4[Fernwartungszugang]
    Übungsbereich(Testserver)---admin99[Fernwartungszugang]
    %% Könnt sein, dass die Klickerei nicht geht, aber das liegt dann am Gitlab.
    click Startseite "https://www.ftcommunity.de"
    click Bilderpool "https://www.ftcommunity.de/bilderpool.html"
    click Downloads "https://www.ftcommunity.de/downloads.html"
    click Forum "https://forum.ftcommunity.de"
    click Chat "https://www.ftcommunity.de/ftcomm8cb8.html?file=chat"
    click ft-pedia "http://www.ftcommunity.de/ftpedia"
    click wiki "https://www.ftcommunity.de/wiki.html"
  end
```

### Wie das mit dem Architekturvorschlag D aussehen könnte
```mermaid
graph TD;
  subgraph Forum
    LoginForum["Login-Modul"];
  end
  subgraph https://www.ftcommunity.de
    Webserver["https://ftcommunity.de (Frontend-Webserver)"];
    Webserver-->|schreiben|ZugangskontrolleBilderpool
    Webserver-->|schreiben|ZugangskontrolleFtpedia
    Webserver-->|schreiben|ZugangskontrolleRest
    IdentityProvider["OpenID Connect Identity Provider"]---|Login prüfen|LoginForum;
    ZugangskontrolleBilderpool[Zugangskontrolle]-->|Inhalte schreiben|GitBilderpool("Git (Bilderpool)")
    ZugangskontrolleBilderpool---|Zugang prüfen|IdentityProvider
    ZugangskontrolleFtpedia[Zugangskontrolle]-->|Inhalte schreiben|GitFtpedia("Git (ft:pedia)")
    ZugangskontrolleFtpedia---|Zugang prüfen|IdentityProvider
    ZugangskontrolleRest[Zugangskontrolle]-->|Inhalte schreiben|GitRest("Git (sonstige Inhalte)")
    ZugangskontrolleRest---|Zugang prüfen|IdentityProvider
    GitBilderpool-->WebsiteGenerator
    GitFtpedia-->WebsiteGenerator
    GitRest-->WebsiteGenerator
    WebsiteGenerator["Website-Generator"]-->StatischeDaten("HTML, CSS und JavaScript (statisch)")
    StatischeDaten-->|lesen|Webserver
  end
```

#### Legende
* Frontend-Webserver: Die zentrale Instanz für die Kommunikation mit dem Rest der Welt. 
  Bedient Lesezugriffe direkt mit statischen Daten, 
  und verteilt Schreibzugriffe als Proxy je nach URL an die passende Zugangskontrolle.
* Zugangskontrolle: Prüft, ob der Zugriff durch einen eingeloggten Benutzer
  mit den passenden Rechten für den jeweiligen Bereich erfolgt. 
  Benutzt dafür den "Identity Provider"
* Git: Bearbeitet den eigentlichen Schreibzugriff. 
  * Führt eventuell noch feinere Rechtekontrollen durch
    (z.B. dass normale Benutzer im Bilderpool nur ihre eigenen Inhalte ändern dürfen)
  * Schreibt und versioniert die Inhalte
  * Triggert den Website-Generator
* Website-Generator: Erzeugt bei Änderungen von Inhalten 
  eine neue Version der statischen Daten
* Identity-Provider: Stellt Benutzerindentitäten und Gruppenzugehörigkeit
  per Open Identity Connect / OAuth 2 für die Zugangskontroll-Module her.
  Benutzt die (existierende) Nutzerdatenbank des Forums und greift darauf
  über das Login-Modul im Forum zu
* Login-Modul: Liefert die Login-Funktion und die Abfrage der Gruppenzugehörigkeit
  von (Forums-)Benutzern in der für den Identity-Provider nötigen Form.

Die meisten Module lassen sich mit fertiger Software umsetzen.
Vorschlag:
* Frontend-Webserver: <https://nginx.org/>
* Zugangskontrolle: <https://github.com/ory/oathkeeper>
* Git: 
  * <https://git-scm.org> (das eigentliche Programm)
  * <https://www.lighttpd.net/> (um Git per HTTP erreichbar zu machen)
  * alternativ <https://gitea.io/> (Vollwertige Weboberfläche für Git inklusive Editor)
* Identity-Provider: <https://github.com/ory/hydra>
* Website-Generator: <https://gohugo.io/>
* Statische Daten: Dateisystem mit Rechteverwaltung (im Betriebssystem integriert)

Die folgenden Komponenten müssen selbst erstellt und gewartet werden:
* Konfiguration für Frontend-Webserver, Zugangskontrolle und Identity-Provider
* Git-"Hooks" für erweiterte Zugriffskontrolle und triggern des Website-Generators
* Templates für den Website-Generator und CSS/JavaScript für die Darstellung 
  und die dynamischen Funktionen (z.B. Galerie mit zufällig ausgwählten 
  Bildern aus dem Bilderpool)
* Eine HTML/Javascript-Oberfläche für den Schreibzugriff auf den Bilderpool
* Eine HTML/Javascript-Oberfläche für den Schreibzugriff 
  auf ft:pedia und den Rest der Website
  (kann eventuell durch Gitea ersetzt werden, hängt von den Anforderungen der
  "Redakteure" ab)
* Das Login-Modul im Forum
