# Sicherheitskonzept: Motivation und Überblick

Generell wollen wir verhindern,
dass ein Bösewicht etwas mit <https://ftcommunity.de> anstellt,
das uns nicht passt.
Bei einer öffentlich zugänglichen Website gibt es zwei Dinge, 
die uns nicht passen
* _Datenklau_: Ein Bösewicht schafft es, 
  Daten zu lesen, die ihn nichts angehen
  (wie z.B. Passwörter, oder Profildaten von Benutzern)
* _Vandalismus_: Ein Bösewicht schafft es,
  die Website gegen unseren Willen zu verändern
  oder sogar die Kontrolle über den Server zu übernehmen,
  um ihn als Spamschleuder oder Kontroll-Server eines Botnetzes zu missbrauchen.

Weil wir hier grundlegende Sicherheitsbetrachtungen anstellen,
gehen wir davon aus, 
dass nicht nur überall Datendiebe und Vandalen lauern,
sondern dass diese Datendiebe und Vandalen auch sehr mächtig sind:
Sie können nicht nur -
wie jeder andere Internetbenutzer auch -
auf <https://ftcommunity.de> zugreifen,
sondern auch Zugangsdaten von echten Benutzern stehlen
und sich sogar Root-Zugriff auf die Server von <https://ftcommunity.de> verschaffen.

Allmächtig sind die Datendiebe und Vandalen aber auch nicht:
Sie müssen sich schon etwas anstrengen, 
um sich Zugangsdaten für einen Benutzer-Account
oder Zugriff auf die Server zu verschaffen.
Und wenn die Beute den Aufwand nicht lohnt,
dann machen sie keinen Finger krumm.

Und außerdem sind nicht alle Datendiebe und Vandalen gleich:
Auch da gibt es kleine Fische, 
die nur günstige Gelegenheiten mitnehmen
(zum Beispiel Spammer, die automatisiert das Web nach Email-Adressen abgrasen)
und große Haie, die zu gezielten Angriffen fähig sind
(zum Beispiel Geheimdienste, die die Fischertechnik-Community infiltrieren wollen).
Und natürlich alles dazwischen.

Das Ziel unseres Sicherheitskonzepts ist es daher auch _nicht_,
<https://ftcommunity.de> vor allen Angriffen zu schützen
(das wäre bei so mächtigen Gegnern unmöglich),
sondern 
* den meisten Bösewichten so viele Steine in den Weg zu legen,
  dass sich der Aufwand für sie nicht mehr lohnt
* den Schaden zu minimieren, 
  wenn ein Angreifer trotzdem erfolgreich durch alle Hürden kommt und
* herauszuarbeiten, ab wann der Aufwand für die Verteidigung für _uns_ zu hoch wird.

## Schutz gegen Datenklau

Der beste Schutz gegen Datenklau ist _[Datensparsamkeit](https://de.wikipedia.org/wiki/Datensparsamkeit)_:
Was gar nicht erst da ist, kann auch nicht wegkommen.

Idealerweise haben wir also überhaupt keine Daten, 
die wir gegen Datenklau schützen müssten.
Leider kommen wir nicht ganz ohne aus:
Um uns z.B. gegen Vandalismus zu schützen,
können wir nicht einfach jedem erlauben die Website zu verändern.
Wir brauchen also eine Benutzerverwaltung,
die es ermöglicht Benutzer zu identifizieren und Rechte zu verwalten.
Und dafür brauchen wir private Daten
wie zum Beispiel Benutzernamen und zugehörige Passworte.

Eine zweite Hürde gegen Datenklau ist _Dezentralisierung_:
Wenn wir die privaten Daten nicht zentral auf einem Server speichern,
sondern verteilt über viele Orte,
dann steigt der Aufwand für den Angreifer.

Leider ist auch das nicht in allen Fällen möglich:
Wenn wir z.B. Benutzernamen und Passwörter ausschließlich auf dem PC der einzelnen Benutzer speichern,
dann haben wir keine Möglichkeit mehr zu prüfen,
ob ein Benutzer beim Login auch tatsächlich das richtige Passwort verwendet.

Für die privaten Daten, die wir zentral speichern müssen, 
können wir als weitere Hürde _[Verschlüsselung](https://de.wikipedia.org/wiki/Kryptographie)_ einsetzen:
Damit treiben wir den Aufwand für den Angreifer noch weiter hoch,
und machen es ihm im besten Fall sogar völlig unmöglich,
mit den geklauten Daten etwas anzufangen.

Und schließlich helfen auch die [sonstigen Schutzmassnahmen](#generelle-schutzmassnahmen) dabei, 
dem Angreifer weitere Steine in den Weg zu legen. 

## Schutz gegen Vandalismus
Gegen Vandalismus helfen die stärksten Verteidigungsmittel aus dem vorigen Abschnitt —
Datensparsamkeit, Dezentralisierung und Verschlüsselung —
leider nicht so viel:
Die öffentlich sichtbaren Daten sind Sinn und Zweck der Website,
deshalb kann man sie weder "einsparen",
noch verschlüsseln.
Und auch eine Dezentralisierung der Daten ist schwierig,
wenn man sie mit einem normalen Browser von einem Server abrufen können soll.

Damit bleibt als erste Verteidigungslinie gegen Vandalismus,
den (Schreib-)Zugriff auf die Daten für die Vandalen so schwer wie möglich zu machen.
Hier helfen wieder die [generellen Schutzmassnahmen](#generelle-schutzmassnahmen),
mit denen der Betrieb des Servers so gut wie möglich gegen Angriffe gesichert wird.

Die generellen Schutzmassnahmen helfen allerdings nicht gegen Angreifer,
die entweder die Zugangsdaten eines Redakteurs gestohlen
oder sich Rootzugriff auf den Server verschafft haben.
So ein Angreifer _kann_ die öffentlich sichtbaren Daten manipulieren.

Deshalb brauchen wir hier als zweite Verteidigungslinie eine effektive Schadensbegrenzung:
Wir müssen zuverlässig und schnell erkennen können,
dass ein Vandale Daten manipuliert hat
und welche Daten er manipuliert hat.
Und wir müssen die Manipulation schnell und zuverlässig wieder rückgängig machen können.

Ein mächtiges Hilfsmittel ist auch hier wieder Kryptographie,
diesmal in Form von _[Signaturen](https://de.wikipedia.org/wiki/Digitale_Signatur)_,
mit denen man Fälschungen von Inhalten stark erschweren kann.
Allerdings sind Signaturen auch kein Allheilmittel:
Sie helfen zwar sehr gut gegen einen Angreifer,
der Zugang — auch Root-Zugang — zum Server hat,
aber nicht gegen einen Angreifer, 
der den Signaturschlüssel eines legitimen Benutzers stiehlt
und damit dann seine eigenen Änderungen signiert.

Hier kommt dann als letzte —
und ebenfalls sehr mächtige —
Verteidigungslinie _Redundanz_ ins Spiel: 
Wenn wir die öffentlichen Daten
(und die Software, und die Konfiguration für den Server)
nicht nur auf dem Server selbst haben,
sondern auch noch mehrere aktuelle Kopien an anderen Orten,
dann können wir Manipulationen einfach durch einen Vergleich feststellen,
und sie auch schnell wieder rückgängig machen 
indem wir eine nicht manipulierte Kopie auf dem Server einspielen.

Und als sehr willkommener Nebeneffekt hilft Redundanz nicht nur gegen Vandalismus,
sondern auch gleich gegen Hardware-Ausfall und 
Benutzerfehler wie versehentliches Löschen von Daten.

## Generelle Schutzmaßnahmen

Bei den generellen Schutzmassnahmen geht es darum,
es einem Angreifer möglichst schwierig zu machen Zugang zum Server zu bekommen.

Dabei hilt auch wieder eine Art Sparsamkeit,
nämlich _Sparsamkeit beim Funktionsumfang_ der Software auf dem Server:
Was gar nicht da ist, hat auch keine Lücken die der Angreifer ausnutzen kann.

Oder als Forderung ausgedrückt:
Wir sollten auf dem Server nur das installieren,
was unbedingt nötig ist um die gewünschten Funktionen zu erfüllen.
Alles weitere ist ein (unnötiges) Sicherheitsrisiko.

In dieselbe Richtung zielt das _[Prinzip der minimalen Rechte](https://en.wikipedia.org/wiki/Principle_of_least_privilege)_:
Das gilt zum einen für die Software selbst,
die mit den minimalen Betriebssystem-Privilegien ausgestattet wird,
die für die gewünschten Funktionen nötig sind.
Aber vor allem gilt es für die Administratoren,
die ebenfalls nur die absolut minimalsten Rechte bekommen sollten,
die sie brauchen um die Software zu betreiben und zu warten.

Aber auch die unbedingt nötige Software hat in aller Regel Lücken,
die als Einfallstor für einen Angreifer dienen können.
Und selbst wenn die Software wunderbarerweise perfekt sein sollte,
kann ein Angreifer immer noch Zugangsdaten von einem legitimen Benutzer —
insbesondere von einem Server-Administrator —
stehlen und sich dadurch Zugang verschaffen.
Wenn wir den [Schutz gegen Datenklau](#schutz-gegen-datenklau) ernst nehmen,
dann ist das für den Angreifer zwar nicht einfach,
aber wir gehen ja hier davon aus dass es immer irgendwie möglich ist.

Die nächste Verteidigungslinie hier ist dann die _Modularisierung_:
Wenn wir die einzelnen technischen Aspekte der Website auf getrennte Module aufteilen,
von denen jedes — wie oben gefordert —
nur Zugang zu den minimalen Resourcen hat,
die es braucht um seine Aufgabe zu erfüllen,
dann zwingen wir den Angreifer,
sich von Modul zu Modul "weiterzuhangeln",
bevor er irgendetwas findet oder anstellen kann, was uns schadet.

Und wenn die Administratoren ebenfalls für jedes Modul getrennte Zugänge haben,
die ebenfalls jeweils nur mit den minimal nötigen Rechten ausgestattet sind,
dann zwingen wir den Agreifer dazu, 
auch hier mehr Aufwand zu treiben
(weil er mehrere Zugangsdaten stehlen muss).
Und der Angreifer hat dabei auch ein höheres Risiko,
entdeckt zu werden.

Die Modularisierung hilft auch bei der dritten Säule der sonstigen Schutzmassnahmen,
der _Wartbarkeit_:
Wenn wir den Server so gestalten,
dass die verwendete Software leicht aktualisiert werden kann,
und nur Software verwenden,
die auch aktiv gepflegt wird,
dann können wir neu entdeckte Lücken in dieser Software schnell schließen,
und sie damit als Einfallstor für Angreifer unbrauchbar machen.
