# Anforderungen für <https://ftcommunity.de>
(Siehe auch: <https://forum.ftcommunity.de/viewtopic.php?f=23&t=5046>)

# Was soll die neue Website können?

* Die vorhandene Seitensammlung (aka Bilderpool) ersetzen
* Serverseitig auf vorhandenen modernen, gepflegten und gut dokumentierten
  Programmpaketen basieren
* Strenge Datenschutzvorgaben erfüllen
* Umsetzung des [Sicherheitskonzeptes](https://gitlab.com/ftcommunity/website/blob/master/doc/security.md)
* Den Admins (und Redakteuren) eine unkomplizierte Verwaltung ermöglichen
* Software updates (Server) ohne komplexes Fachwissen ermöglichen
  (also zumindest ohne spezielle Anpassungsfummeleien an vielen verschiedenen
  Stellen)
* Inhalte und Darstellung getrennt verwalten (Bilder in 'ner Art Datenbank, Seitenaussehen in Stylesheets oder wie heißt das?)
* Moderne Browser unterstützen (Firefox, Chrome, ...)
* Smartphone und PC Nutzer ansprechen
* Die Nutzer weder analysieren noch ausspionieren 
* keine Nutzerprofile
* wenn schon Statistiken zur Serveroptimierung, dann bitte anonym. 
* ft:pedia-Download-Statistiken (Dateiname, Ausgabe-Jahr, Ausgabe-Quartal, Downloadzeitpunkt, Anzahl)
* Das gewohnte Logo verwenden
* Ein API für Anwendungen wie den ft Community Publisher anbieten
* So einfach wie möglich sein
* Auch neue Nutzer ansprechen
* gut lesbar sein wie die bisherige Seite,
    * keine dünne graue Schrift auf weißem Hintergrund
    * besser schwarz auf weiß
* Hintergrundfarbe(n), Schriftart und Schriftgröße individuell anpassen
* Blau für Links wie das Blau des ftc-Logos 

Achtung, ab hier könnte es off-topic werden oder schon in die Verfeinerungen abdriften:
# Einteilung in Bereiche 

* ftc-Bereich
    * Startseite
    * Veranstaltungshinweise
    * Impressum
    * Datenschutzerklärung
* ft:pedia 
* Nutzerinhalte
    * Bilder / Alben (Bilderpool)
    * Downloads
    * wiki (von Nutzern angelegte und gepflegte Seiten zu bestimmten Themen)
* Forum
* ft-Datenbank
* Chat ??? (siehe <https://forum.ftcommunity.de/viewtopic.php?f=23&t=5043>)
* Benutzerverwaltung

# Neue Anforderungen
Aus den Erfahrungen mit dem Prototypen ergeben sich einige neue Anforderungen,
die nicht in Vergessenheit geraten sollen:
* Beim Upload von Download-Dateien muss das System sicherstellen, 
    dass nicht schon eine Datei mit diesem Namen existiert.
    Dateien müssen einen eindeutigen Namen haben.
* Der Benutzer, der einen Text hochgeladen hat, muss diesen auch editieren können.

# Anforderungen nach Bereichen
* __Startseite__: Zeigt bei jedem Zugriff eine andere Auswahl von Bildern
  aus dem Bilderpool an.
    (also ein Zufallsbild, das neueste Bild, ddas mit dem neuesten Kommentar)
  Anzeige umfasst Kategoriename, Konstrukteur, Bild und beide Angaben sind
  als Links ausgeführt und öffnen genau dieses Bild.
  Grober Vorschlag zum Aussehen, siehe
  https://forum.ftcommunity.de/viewtopic.php?f=23&t=5003&p=36605#p36605
  * Der Nutzer muss nicht angemeldet sein.
  * Vorstellung von Verein und Vorstand (mit Bild)
  * Termine
  * Links 
    * aktuell halten
    * sollen sich beim Anclicken in neuer Registerkarte öffen
 * __Bilderpool__
    * Felder für jedes Bild:
        * Titel
        * Name des Konstrukteurs
            * den Konstrukteuren in der Bildunterschrift Links zuweisen. So könnte man weitere Modelle des selben Nutzers schnell auffinden.
        * Name des Fotografen
        * Erstellungsdatum
        * Lizenz (eine der Creative Commons-Lizenzen oder "Alle Rechte vorbehalten")
        * Identifier für ein Modell 
            (damit mehrere Fotos eines Modells als zusammen gehörig gekennzeichnet werden)
        * Zuordnung zu einer Serie
        * Tags
        * Hochgeladen von ... am ...
            * nicht machen, 
              weil dadurch der Benutzername zwangsläufig veröffentlicht und mit den Bildern verknüpft wird.
            * Verknüpfung von Bildern mit Benutzer, für späteres Löschen
    * Erlaubt Suche nach Bildern, Themen, Stichwort(en), Upload-Datum,
      Fotograf, Konstrukteur, ...
    * Kategorien mit der genau richtigen Struktur und Tiefe
    * angemeldete und freigeschaltete Benutzer können
      * Bilder hochladen
      * hochgeladene Bilder editieren (Gammakorrektur und so Zeug)
        * Verkleinerung der Bilder beim Hochladen
      * Kommentare zu Bildern schreiben
      * eigene Kommentare editieren
      * selbst angelegte Kategorie(n) editieren
      * registrierte Benutzer erhalten Benachrichtigung, sehr ähnlich dem
        Forum, bei
        * neuem Kommentar
        * neuem Bild in bestimmter Kategorieauswahl (inkl. "jedes neue Bild")
      * wählen ob neue Bilder im Suchergebnis komplett angezeigt
         werden (so wie heute) oder lediglich die Kategorie angezeigt wird 
         (mit dem letzten neuen Bild dazu).
         _Mich ärgert der gelegentliche Massenupload. Kommen da mehrere
         zusammen,
         fällt das eine feine Bild vom Kollegen der nicht bei der Convention
         war zwischendrin gar nicht mehr auf oder ich klicke mich ewig durch
         die ewig vielen Seiten ..._
    * Angemeldete Benutzer müssen ihre Bilder nicht erst freischalten lassen.
      Bei Regelverstössen kann diese Option für Nutzer durch Admins erzwungen
      werden.
    * Batch-Upload (als Zip-Datei) von Bildern
        * Der Nutzer soll beim Hochladen seine Bilder ordentlich dokumentieren.
        * API für ft:publisher
* __ft:pedia__
    * Hauptseite der ft:pedia üfer "ftpedia.de" erreichbar
    * Anzeige der Cover mit Link (Downloadmöglichkeit) als Gesamtübersicht
        * zeilenweise nach Jahrgängen gruppiert, die neuesten oben
    * Anzeige Cover aktuelles Heft in Navigationsleiste; 
    kurz vor Neuerscheinen: "verschwommenes" Ankündigungscover
    * Link auf Nationalbibliothek, Link auf Forums-Rubrik ft:pedia
    * Seite mit Tabelle und verlinkten Einzelbeiträgen
    * Offline-Übersicht (csv), 
    * wenn möglich: Hosting des "pdf-Generators" von Olagino (https://forum.ftcommunity.de/viewtopic.php?f=31&t=4992) 
    zur Erzeugung von pdf-Sammlungen ausgewählter Beiträge (bpsw. Serientexte)
    * Zugriff auf Download-Statistiken wie bisher
    * Menü 2018 oben und dann runter bis 2011
* Admins/Redakteure können die Website editieren
* Es gibt eine __Benutzerverwaltung__ mit
  * Login/Logout
  * (Selbst-)Registrierung von Benutzern - mit modernstem Botschutz
  * (Selbst-)Verwaltung von Profildaten durch angemeldete Benutzer
  * Mandatory: eMail-Adresse, Nickname - mehr nicht, der Rest ist Option!
  * Begrenzte Anpassungen der Startseite individuell für angemeldete
    Benutzer
    (z. B. Neueste Bilder anstelle der Zufallsauswahl oder die neuesten 5
    Einträge im Forum, oder ...)
  * Vergabe/Verwalten von weitergehenden Rechten durch Admins
    * Schreibrecht im Bilderpool
    * Schreibrecht für die ganze Website ("Redakteur")
    * Schreibrecht für ft:pedia
  * Zugang zur Rechteverwaltung ("Admin")
  * In einer späteren Ausbaustufe soll das [Forum](https://forum.ftcommunity.de) 
    dieselbe Benutzerverwaltung mit nutzen können ("Single Sign On")
  * Die Verknüpfung der neuen Nutzer-IDs mit den alten IDs ist wünschenswert.

# Zusätzliche Anforderungen
* Übernahme von Daten aus dem alten System
    * Bilder und Videos
    * Texte (Bildbeschreibungen und Kommentare)
    * Benutzerdaten
    * Downloads
    * wiki-Seiten
    * die Mirror-Page von ft-computing und ähnliche Schätze
    * ft:pedia

 Die URLs zu Kategorien, Bildern und ft:pedia sollten möglichst bleiben!
 Die Struktur der URLs wird einem [Dokument](urls.md) erläutert.
 
 ## Umsetzung
 Zum Import der Downloads aus dem alten System dient ein [Python-Skript](import-files.py)

# Offene Fragen
## Tags
Eine Hierarchie von Kategorien darf weder zu grob sein (zu viele Bilder zu einem Begriff),
noch zu fein sein (Kategorien, die leer sind oder nur wenige Bilder enthalten).
Die Kategorien und ihre Beliebtheit lassen sich allerdings schlecht vorhersehen.
Jedes Bild kann zu mehreren Kategorien gehören.
Daher wird die Verwendung von Tags (mehrere beschreibende Begriffe pro Bild) bevorzugt.
Hierbei gibt es mehrere Möglichkeiten:
* Verwendung einer Liste von Schlüsselwörtern
* Freie Vergabe von Begriffen durch die Nutzer

Bei Freitext für Begriffe kann sich das Problem ergeben, 
dass Nutzer gleiche Konzepte unterschiedlich benennen ("Nord-Convention" vs. "Nordconvention").
Eine intelligente Suche (Elastic search) kann dieses Problem umgehen.

* Soll es bestimmte Kategorien geben? 
* Wenn ja, welche?
* Sollen bestimmte Schlüsselwörter zur Auswahl vorgegeben werden ?
* Muss der Nutzer Schlüsselwörter vergeben ?
* Muss der Nutzer eine Zuordnung zu bestimmten Kategorien vornehmen (z.B. Pneumatik Ja/Nein) ?
* Soll der Nutzer Schlüsselwörter frei eingeben können?

## Ansprache der Benutzer?
* Direkt
    * Du ("Du kannst hier Bilder finden.")
    * Sie ("Sie können hier Bilder finden.")
* Indirekt ("Hier finden sich viele schöne Bilder.")

## Darstellung von Bildern aus dem Bilderpool auf der Startseite
* Sollen Bilder aus dem Bilderpool auf der Startseite dargestellt werden?
* Wenn ja:
    * Zufällige Bilder?
    * Neueste Bilder?
    * Zuletzt kommentierte Bilder?
    * Von einem Redakteur ausgewählte Bilder?

# Abgelehnte Anforderungen / Könnte später kommen
## Exif-Daten im Bilderpool
* Verstößt gegen Gebot der Datensparsamkeit
* Wenn der Nutzer selbst wählen kann, ob diese Daten genutzt werden sollen, wird es zu kompliziert.

## Marker für Beschriftung der Modelle in Abbildungen
* Kann der Fotograf bei der Bearbeitung des Bildes auf eigenem Rechner erzeugen

## Keine Größenbeschränkung der Bilder
* Bei aktuell ca. 40.000 Bildern ist eine Größenbeschränkung der einzelnen Datei nötig.


## Vernetzung von Bilderpool und Forum (z.B. Kommentare als Thread im Forum)

## Wiki
Statt einer Wiki-Struktur sollten Redakteure und schreiblustige Benutzer
eigene Seiten zu bestimmten Themen anlegen und pflegen können.
Statt eines Wikis mit nur wenig Inhalt
gibt es dann Seiten unter "Know-How".





