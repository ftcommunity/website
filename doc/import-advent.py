#!/usr/bin/env/ python3
# Für den automatischen Import des Adventskalenders aus der Datenbank auf ftcommunity.de.

# Die Daten für die Datenbankanbindung sind aus Sicherheitsgründen gelöscht.

import pymysql.cursors
import sys

try:
    conn = pymysql.connect(host='',
                           user='',
                           password='',
                           db='ftcommunity',
                           charset='utf8mb4',
                           cursorclass=\
                               pymysql.cursors.DictCursor)

    with conn.cursor() as cur:
        sql = 'SELECT * from advent'
        cur.execute(sql)
        result = cur.fetchone()
        while result:
            print ('ID=%d tag=%d dateiname=%s' % (result['id'], result['tag'], result['dateiname']))
            filename1 = 'results/' + str(result['tag']) + '.txt'
            f1 = open(filename1, 'wt')
            f1.write(result['inhalt'])
            f1.close()
            if result['dateiname'] != None:
                filename2 = 'results/' + result['dateiname']
                f2 = open(filename2, 'wb')
                f2.write(result['download'])
                f2.close
            result = cur.fetchone()
    
except BaseException as ex:
    print('Fehler:', ex)
    print(sys.exc_info()[0])
finally:
    conn.close()
