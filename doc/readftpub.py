#!/usr/bin/env/python3

#Liest eine ftpub-Datei aus (XML)
# Aufruf-Argumente: Verzeichnis (mit / am Ende) ftcpub-Dateiname

#import xmltodict
import xml.etree.ElementTree as ET
import xml.dom.minidom
import csv
import sys
from pathlib import PureWindowsPath

def getimagedata(bild, picturequantity, no):
    bildtitle = bild.find('Title').text
    bilddesc = bild.find('Description').text
    bildphoto = bild.find('Photographer').find('Name').text
    bildconst = bild.find('Constructor').find('Name').text
    bildorder = int(bild.find('PublishingOrder').text) + 1
    if bildfileprefix:
        if (picturequantity > 9) and (no < 10) :
            bildfile = bildfileprefix + '0' + str(no) + '.jpg'
        else:
            bildfile = bildfileprefix + str(no) + '.jpg'
    else:
        bildfile = PureWindowsPath(bild.find('FileName').text).name
    upload_writer.writerow([bildtitle, bilddesc, bildphoto, bildconst, bildorder, bildfile])


def output(node, tiefe):
    #tab = ''
    #for i in range(tiefe):
        #tab += '    '
    # print(tab, node.tag, node.text)
    for baby in node:
        output(baby, tiefe+1)
#    if node.tag == 'Images':
#        for bild in node.findall('ImageInfo'):
#            if bild.find('Publish').text == 'true':
#                getimagedata(bild)
#    for bild in node.findall('Image'):
#        if bild.find('Publish').text == 'true':
#            getimagedata(bild)
    if node.tag == 'Images':
        picturelist = node.findall('ImageInfo')
    else:
        picturelist = node.findall('Image')
    picturequantity = len(picturelist)
    runningnumber = 1
    for bild in picturelist:
        if bild.find('Publish').text == 'true':
            getimagedata(bild, picturequantity, runningnumber)
            runningnumber = runningnumber + 1 
 
dirname = sys.argv[1]
pubname = sys.argv[2]
with open(dirname+pubname) as xmldata:
    xml = xml.dom.minidom.parseString(xmldata.read())  # or xml.dom.minidom.parseString(xml_string)
    xml_pretty_str = xml.toprettyxml()
    print(xml_pretty_str)

with open(dirname+'liste.csv', mode='w') as upload_file:
    upload_writer = csv.writer(upload_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    upload_writer.writerow(['Titel','Beschreibung','Fotografen','Konstrukteure', 'weight', 'picture']) 
    tree = ET.parse(dirname+pubname)
    root = tree.getroot()
    cattitle = root.find('Title').text
    catdesc = root.find('Description').text
    
    bildfileprefix = None;
    for sss in root.findall("./PublisherSettings/PublisherSetting/Parameters/PublisherSettingValue"):
        if sss.find('Name').text == 'Prefix':
            bildfileprefix = sss.find('Value').text
    if bildfileprefix == None:
        print('nix')
    else: 
        print(bildfileprefix)    
    for child in root:
        output(child, 0)

titelfile = open (dirname+'Titel', 'w')
titelfile.write(cattitle)
titelfile.close()
descfile = open (dirname+'Beschreibung', 'w')
descfile.write(catdesc)
titelfile.close()




