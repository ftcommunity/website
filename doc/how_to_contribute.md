# Ich möchte hier mitmachen - wie kann ich mich beteiligen?

* Mitmachen heißt, dass Du Dokumente erstellen oder bearbeiten möchtest,
  an Diskussionen teilnimmst oder aktiv ins Geschehen eingreifst.
  Lesen darf grundsätzlich jeder anonyme Besucher.
  Zum Mitmachen benötigst Du ein Konto bei Gitlab oder Du nutzt Dein schon
  vorhandenes Konto bei Github, Bitbucket oder einem sozialen
  Netzwerk, um Dich anzumelden.
  Schau einfach mal im "Sign in" / "Register"-Bereich des Gitlab, was da
  aktuell im Angebot ist.
  Falls Du kein passendes Konto hast oder nicht mit Gitlab assoziieren
  möchtest, registrierst Du Dich per
  ["Register"](https://gitlab.com/users/sign_in), 
  auch per Klick rechts oben hier auf der Seite erreichbar.
  Das erfordert meiner Erfahrung nach Englischkenntnisse - und natürlich
  eine gültige eMailadresse.
* Möchtest Du einen Beitrag schreiben, gibt es zwei Möglichkeiten:
  1. per Browser
  2. per lokalen Tools (z. B. GitBash)
* Die verteilte Arbeitsweise erfordert ein wenig Disziplin.
  **Die (wenigen) Regeln die es gibt, müssen eingehalten werden.**
  Das ist die erste Regel und ich muss hoffentlich nicht erklären wieso.
* Wenn Du ein eigenes Konto eingerichtet hast, kannst Du da einen eigenen
  Datenspeicher (_Repository_) anlegen und darin herumspielen.
  Das gibt Dir Gelegenheit Datensalat anzurichten und zu lernen, wie Du mit
  dem Gitlab zu Rande kommst.

Wer lokale Git-Tools bereits kennt, benötigt wohl keine großartige Anleitung
an dieser Stelle und springt direkt zum Abschnitt
[Richtiges edit](#richtiges edit).
Neulinge nehmen lieber den Browser für's edit und lesen erstmal weiter.

Sobald Du angemeldet bist, musst Du noch die Erlaubnis zur Mitarbeit am
Projekt beantragen.
Dazu gehst Du nochmal zur Projektseite https://gitlab.com/ftcommunity/website
zurück und betätigst den zugehörigen Button "Request Access" rechts oben etwas
unterhalb der Projektkopfzeile.
Einer der Projektgurus bekommt die Anfrage und wird Dir die Freigabe geben.
Bitte hab' etwas Geduld, es könnte auch mal einen Tag dauern.
Du bekommst vom Gitlab eine eMail, sobald der Vorgang beendet ist.

Wenn Du die Mitmacherlaubnis erhalten hast, kannst Du endlich loslegen.
Du darfst die vorhandenen Dokumente per Browser bearbeiten und speichern.
Du darfst Dateien oder Ordner (oder beides) hinzufügen, löschen, Inhalte
verändern; kurz alles was Du auf Deinem Computer normalerweise auch machen
darfst.
Dazu kommt die Beteiligung an den Diskussionen, die gegenwärtig sogar
noch erheblich sinnvoller ist, als die Änderung von Dokumenten.

Aus Sicherheitsgründen werden Deine Änderungen in einem getrennten
Bereich zwischengespeichert.
Das nennt sich im "Neusprech" _Branch_ (und zu gut deutsch _Zweig_).
Auf diesem _Branch_ kann das Dokument weiter und weiter bearbeitet werden.
Gitlab fragt im Zweifelsfall nach einem _Branch_namen und macht auch gleich
einen Vorschlag.

Sogar mehrere Benutzer können zeitgleich im gleichen _Branch_ an Dokumenten
herumwerkeln, Gitlab kümmert sich um die meisten daraus entstehenden Probleme
und löst sie automatisch auf.
Irgendwann ist der Punkt erreicht, an dem der Inhalt des Dokumentes als fertig
angesehen wird.
Um nun das Dokument in den allgemeinen Pool zu übernehmen, gibt es den _Merge
request_ (kurz: MR).
Der wird gestellt und einer der erfahrenen Benutzer wird sich der Sache
annehmen.
Er wird den Inhalt der Datei prüfen und entscheiden, ob Deine Arbeit so okay
ist oder noch ein paar Änderungen benötigt.
Wenn die Feinheiten der Nacharbeit erledigt sind, wird der _merge_
durchgeführt.
Der _Branch_ wird auf den besonderen _Branch_ "master" übernommen und die
Änderung steht ab da offiziell zur Verfügung.

Während der _Merge request_ offen ist, wird meist auch etwas diskutiert.
Geht es um Gitlab oder organisatorische Themen, wirst Du eventuell weniger
beitragen.
Geht es hingegen um echte Inhalte der neuen Webseite, ist Deine Meinung
besonders erwünscht.

Gibt es Probleme oder Du hast versehentlich etwas falsch gemacht, ist das
nicht weiter schlimm: gerade durch die Nutzung von Git kann nahezu alles wieder
rückgängig gemacht werden.
Also keine Angst vor Fehlern!
Versuch nur sie bitte nicht zweimal zu machen.

## Edit per Browser

Gitlab ermöglicht Dir eine recht komfortable Arbeitsweise.
Um eine Datei zu ändern, musst Du zunächst wissen wie sie heißt und wo sie
zu finden ist.
Das ist meist der etwas trickreichere Teil für den Anfänger.
Der Text, den Du gerade liest, findest Du in der Datei "how-to-contribute.md"
im Ordner "doc".

Du hast bestimmt schon auf der Startseite die Anzeige bemerkt, in der derzeit
ein Ordner "doc" auftaucht und eine Textdatei "README.md".
Klickst Du nun auf "README.md", wechselt die Ansicht ein bisschen und Du kommst
zur Seite, auf der Du die Datei einzeln angezeigt bekommst.
Dort findet sich eine Anzahl von diversen Buttons (die Dinger zum Anklicken ;)
), einer davon heißt _Edit_.
Klick darauf und Du siehst den Text in einer anderen Darstellung, sogar farbig
markiert.
Jetzt kannst Du mit der Arbeit am Text loslegen.

Dabei gibt es auch noch ein paar Regeln zu beachten, aber die kommen erst
weiter unten.
Bitte lies diesen Abschnitt unbedingt auch noch, bevor Du wirklich anfängst.

In der Eingabemaske sind nun nicht viele Möglichkeiten anzuwählen.
"Write" ist das Fenster zum editieren, "Preview" zeigt, wie das dann hinterher
aussieht.
Zwischen den beiden Ansichten kann beliebig gewechselt werden.

Nun zum wichtigeren Teil:
Unter dem Eingabefeld für den Text sind noch ein paar weitere Eingabefelder
angebracht.
Ist die Arbeit am Dokument für heute beendet und Du möchtest morgen
weitermachen, nimmst Du zuerst den Haken bei "Start a **new merge request**
with these changes" heraus.
Mit dem gesetzten Haken erklärst Du Gitlab, dass Du die Arbeit komplett
beendet hast und diese Änderung ins Repository übernommen werden soll.

Die "Commit Message" darf knapp erklären um was es bei der Änderung geht,
z. B. "Alte Tipfehler korrigiert und neue gemacht."
Der "Target Branch" ist dann der Vorschlag, wohin die Änderung
zwischengespeichert werden soll.
Dieser Name darf vom Benutzer geändert werden, muss aber nicht.

Ist das soweit alles geklärt, sendet ein Klick auf "Commit changes" die
Änderungen ab, löst den eventuellen _Merge request_ aus und so weiter.
Ein "Cancel" verwirft die Änderungen unwiederbringlich!
Ebenso wenn man die Seite verlässt.
In dem Fall kommt aber noch eine Abfrage
"This page is asking you to confirm that you want to leave - data you have
entered may not be saved."
"Leave" kommt dem "Cancel" gleich, "Stay" gibt Dir die Chance die Daten doch
noch zu retten.

Soll die Änderung in einem existierenden _Branch_ erfolgen, kann **vor** dem
editieren der _Branch_ gewechselt werden.
Dazu wählst Du oben, wo "master" steht, den vorhandenen _Branch_ aus.
Das geht per Aufklappmenü und zeigt alle vorhandenen _Branches_ an.
Deine _Branches_ solltest Du sofort in der Auswahl wiedererkennen können.

Natürlich funktioniert diese Vorgehensweise mit allen anderen Dateien
entsprechend.

## Richtiges edit

Für eine angenehme Zusammenarbeit, und insbesondere um die _Merge requests_
für die Bearbeiter angenehm zu gestalten, sei Dir empfohlen Dich tunlichst
an diese Regeln zu halten (egal wie das aussieht):
* **Nach spätestens 80 Zeichen erfolgt ein Zeilenvorschub (ENTER-Taste).**
  Im Browser gibt es da einen ziemlich blassen senkrechten Strich zur
  Orientierung.
  Wer die lokale Arbeitsweise bevorzugt, hat meist auch einen
  Texteditor der sowas besser kann oder gar die Zeilenumbrüche automatisch
  einbaut.
  Das ist zunächst gewöhnungsbedürftig (und es mag gut begründete Ausnahmen
  geben) hat aber mit den _Difftools_ zu tun, die vom Adressaten des _Merge
  request_ eingesetzt werden.
* **Keine Trennstriche verwenden.**
  Wörter die nicht mehr vollständig in eine Zeile passen, kommen komplett in
  die nächste Zeile.
* **Ein neuer Satz beginnt in einer neuen Zeile.**
* **Ein neuer Gedanke wird mit einem weiteren Zeilenvorschub vom
  vorhergehenden Textabschnitt abgesetzt.**
  In der aufbereiteten Browserdarstellung ergibt das dann auch einen Absatz.
* **Vorhandene Textumbrüche, Tabulator- oder Leerzeichen werden nicht
  angefasst wenn nicht durch Änderungen am Text eine der obigen Regeln diese
  Neuformatierung erfordert - egal wie sehr die Gestaltung in der Datei Dein
  Auge oder ästhetisches Empfinden beleidigt.**
  Auch das hat mit dem _Merge request_ und dem _Difftool_ zu tun.
  Das Difftool markiert jede Änderung gegenüber der vorhandenen Version.
  Das Difftool kann allerdings nicht zwischen wichtiger inhaltlicher Änderung
  oder stupider Umformatierung durch entfernen doppelter Leerzeichen
  unterscheiden.
  Diese Unterscheidung bleibt am Bearbeiter des _Merge request_ hängen und
  kostet sie oder ihn unnötig Zeit, Nerven und Kaffee ;) um die wenigen
  wichtigen Änderungen aus eventuell ganz vielen unwichtigen und irrelevanten
  Änderungen herauszusieben.

## Kleiner markdown-Leitfaden

Das was da in der Textdatei steht (.md sind nämlich nur Textdateien wie .txt
oder .asc auch) wird von speziellen Erweiterungen im Gitlab in eine optisch
ansprechende Darstellung gebracht, Neusprech: _gerendert_
Diese Konvertierung ersetzt dabei sogenannten _whitespace_, das sind
beispielsweise Leerzeichen, Tabulatorzeichen und Zeilenvorschübe, selbsttätig
durch Leerzeichen oder Zeilenvorschübe und passt die Darstellung an die
aktive Fensterbreite an.

Fettschrift, kursiver Schriftschnitt und viele weitere Details der Darstellung
werden aus einfachen (und meist gut zu merkenden) Textelementen erzeugt.
Am Ende bekommt der Browser vom Gitlab html-Code der aus dem Textdokument
erzeugt wurde.
Der Vorteil dabei: Du musst nicht html beherrschen um ein durchaus gut
lesbares Dokument mit optischer Gestaltung zu erzeugen.
Und Du kannst das rohe Textdokument fast genau so gut lesen, falls mal kein
Browser greifbar ist.

Wir verwenden hier bislang nur wenige Dinge.
Überschriften, Listen und Textformate wie _kursiv_ oder **fett**.

### Kursive Schrift
Kursive Schrift wird durch einen Unterstrich oder einen Asterisk direkt vor
dem ersten Buchstaben eingeleitet und ebenso direkt hinter dem letzten
Buchstaben beendet.
Ganze Wörter oder auch Textabschnitte können so hervorgehoben werden.
In diesem speziellen Dokument soll kursive Schrift gittypische Begriffe
hervorheben.
`_kursiv_` oder `*kursiv*` wird in der Browserdarstellung zu _kursiv_.

### Fettschrift
Fette Schrift (bold) wird durch zwei Unterstriche oder zwei Asterisks
eingeleitet und beendet.
`__fett__` oder `**fett**` ergibt __fett__.
Welche Version Du benutzt, ist Geschmackssache. Ich persönlich leite kursiv
gerne mit einem Unterstrich ein, nehme aber die Asterisks für Fettschrift.

### Überschrift
Hier sind es vorangestellte Hashes (Rautezeichen), die die Überschrift als
solche definieren.
Die Anzahl der Hashes definiert die Ebene der Überschrift.

### Link auf Abschnitt im gleichen Dokument
Der ist in der Doku kaum zu finden aber eigentlich gar nicht kompliziert.
Zuerst kommt eine eckige Klammer auf. Dann ein Wort oder sogar ein ganzer
Text. Als nächstes eine eckige Klammer zu. Danach sofort eine runde Klammer
auf und ein Hash, gefolgt von dem exakten Text (z. B. eine Überschrift) und
einer runden Klammer zu.
`[Wohin die Reise geht](#Überschrift)` erzeugt [Wohin die Reise geht](#Überschrift)
als Link zur Syntax der Überschrift(en).

### Zeilenvorschub
Üblicherweise ist es ausreichend die einfachen Möglichkeiten zu nutzen und
Fließtext zu erzeugen.
Manchmal ist es jedoch nötig einen extra Zeilenvorschub zu setzen, zum
Beispiel um einen Satz in einer neuen Zeile beginnen zu lassen ohne einen
neuen Absatz zu erzeugen.
Dazu sind am Zeilenende mindestens zwei
Leerzeichen zu setzen, mehr ist nicht verboten, muss aber auch nicht sein.
Der Haken dabei: Texteditoren zeigen Leerzeichen meist nicht an, man muss
diese Option erst aktivieren (und sich daran gewöhnen).
Im Browser werden Leerzeichen nicht sichtbar dargestellt und diese
spezielle Formatierung fällt beim Edit leider nicht auf.
Am Besten versuchst Du ohne dieses Schmankerl auszukommen.

Für Beispiele zur Formatierung schaust Du per _Edit_ in diese Datei oder lädst
sie Dir per _Download_ auf Deinen Computer und siehst per Texteditor nach.

<!-- Das ist ein Kommentar. Da hat markdown keine eigene Syntax für, aber
die Originalsyntax vom html kann man sich ja durchaus merken. Und wenn mal
irgendwas mit markdown nicht so geht wie es sollte, kann immer und überall
html untergemischt werden.
Kommentare sollten aber extrem sparsam verwendet werden, nehmen sie doch
den Passus komplett aus der fertigen Darstellung raus! -->

Wenn Du es genauer wissen möchtest und vor englischen Webseiten nicht
kapitulieren musst, dann schau Dich mal hier um, bitte:
https://docs.gitlab.com/ee/user/markdown.html
und natürlich auch gerne dort wo Deine Lieblingssuchmaschine Dich auf der
Suche nach "markdown" hinschickt.