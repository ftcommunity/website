# Für den automatischen Import von Files aus der noch aktuellen statischen Seite ftcommunity.de.
# Soll Markdown-Files aus den Tabellen erstellen
# und die Dateien an die richtige Stelle kopieren.
# Die URL und das Verzeichnis, unter dem die Dateikopien liegen,
# werden als Konstante angegeben und müssen angepasst werden.
#
# Gibt für Files, deren Namen sich nur in der Extension unterscheiden,
# einen Fehler bei Hugo, weil die Datei schon existiert,
# und ersetzt dann fehlerhaft.
# Abhilfe: bis jetzt manuelle Reparatur dieser Fälle nötig.
#
# Start im Repository-Hauptverzeichnis, also hier ~/git/website-hugo-poc


import requests
import lxml.html as lh
import pandas as pd
import os
import subprocess
import shutil
import re
from datetime import date, datetime

hugoexe='<das Programmverzeichnis von hugo>'
# Es wird angenommen, dass die Files in einem lokalen Backup liegen
backup='das Backupverzeichnis'
hugocontent='Hugos Daten/content/'
url='https://ftcommunity.de/downloads6fd1.html?kategorie=Gemeinschaftsprojekte'
target='knowhow/umzugsgut/gemeinschaftsprojekte/'


#aus: https://towardsdatascience.com/web-scraping-html-tables-with-python-c9baba21059
#angepasst
#Create a handle, page, to handle the contents of the website
page = requests.get(url)

#Store the contents of the website under doc
doc = lh.fromstring(page.content)

#Parse data that are stored between <tr>..</tr> of HTML
# tr_elements = doc.xpath('//tr')
# für die Tabellen, über denen noch andere Tabellen stehen, versuchen wir was anderes
tr_elements = doc.xpath('//table[@class="downloadtable"]/tr')
#print(len(tr_elements))

#Create empty list
col=[]
i=0

#wichtig für uns: die Links zu den Files
# alllinks = doc.xpath('//td/a/@href')
# hier auch für mehrere Tabellen auf einer Seite
alllinks = doc.xpath('//table[@class="downloadtable"]/tr/td/a/@href')
links=[]

#Links zu wo falsches hin nicht übernehmen
for i in range(0,len(alllinks)):
    if ((alllinks[i]).startswith('data/downloads')):
        links.append(alllinks[i])

#For each row, store each first element (header) and an empty list
for t in tr_elements[0]:
    i+=1
    name=t.text_content()
    col.append((name,[]))
col.append(("Link",[]))
    
#Since our first row is the header, data is stored on the second row onwards
for j in range(1,len(tr_elements)):
    #T is our j'th row
    T=tr_elements[j]
    
    #If row is not of size 6, the //tr data is not from our table 
    if len(T)!=6:
        break
    
    #i is the index of our column
    i=0
    
    #Iterate through each element of the row
    for t in T.iterchildren():
        data=t.text_content()
        #Append the data to the empty list of the i'th column
        col[i][1].append(data)
        #Increment i for the next column
        i+=1

    col[i][1].append(links[j-1])

Dict={title:column for (title,column) in col}
df=pd.DataFrame(Dict)


#Erstellen des passenden Markdownfiles und Kopieren des Files
for index, row in df.iterrows(): 
    filename = row['Link']
    basename = os.path.basename(filename)
    #tut nicht bei mehr als 1 Punkt
    #name, extension = basename.split('.')
    #daher so:
    name, extension = os.path.splitext(basename)
    extension = extension.strip('.')
# Abfrage ob Dateiname %20 als Ersatz für Leerzeichen enthält
    if (filename.find('%20') != -1):
# wenn ja: ersetzen einmal durch ursprüngliche Zeichen
        sourcename = filename.replace('%20', ' ')
        sourcename = sourcename.replace('%2c', ',')
        sourcename = sourcename.replace('%26', '&')
# andererseits ersetzen durch _
        name = name.replace('%20', '_')
        name = name.replace('%2c', '')
        name = name.replace('%26', '')
        basename = name+'.'+extension
        subprocess.Popen([hugoexe,'new','-k','download',target+name+'.md']).wait()
        shutil.copy(backup+sourcename, hugocontent+target+name+'.'+extension)
        mdfilename = hugocontent+target+name+'.md'
    else:
# wenn nicht, normal weiter
#Hugo startet in einem Subprocess, auf dessen Ende wir einen Moment geduldig warten
        subprocess.Popen([hugoexe,'new','-k','download',target+name+'.md']).wait()
        shutil.copy(backup+filename, hugocontent+target)
        mdfilename = hugocontent+target+name+'.md'
    
#Jetzt müssen in dem MD-File noch die richtigen Frontmatterparameter gesetzt werden
    fin = open(mdfilename, "rt")
    data = fin.read()
    fin.close()
    
    data = re.sub(r"title: .*",'title: "'+row['Bezeichnung']+'"',data)
    data = re.sub(r'file: .*','file: "'+basename+'"',data)
    data = re.sub(r'date: .*','date: "'+(datetime.strptime(row['Datum'],"%d.%m.%Y")).strftime('%Y-%m-%d')+'T00:00:00"',data)
    if row['Autor']: 
        data = data.replace("KONSTRUKTEUR OPTIONAL", row['Autor'])
        data = data.replace("UPLOAD", row['Autor'])
    else:
    #für fehlende Angaben des Autors gibt es Default-Werte
        data = data.replace("KONSTRUKTEUR OPTIONAL", "-?-")
        data = data.replace("UPLOAD", "-LegacyAdmin-")
    data = data.replace("LEGACY OPTIONAL", '/'+filename)
        
    fin = open(mdfilename, "wt")
    fin.write(data)
    fin.close()
    
#Content gibt es auch noch    
    fin = open(mdfilename, "a")
    fin.write('<!-- https://www.ftcommunity.de/'+filename+' -->\n')
    fin.write(row['Beschreibung'])
    fin.close()

    
                        
