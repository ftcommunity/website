# Suche mit Solr

Nach den ursprünglichen Plänen sollte die Suche auf der ftc-Website mit lunr realisiert werden.
Bei diesem Ansatz erstellt Hugo eine Index-Datei (`index.json`),
die dann auf den Client ausgeliefert wird und dort durchsucht wird 
(Suchfeld auf der Startseite, Suche mit JavaScript `search.js`)
Aufgrund der Datenmenge ist dieser Ansatz nicht durchführbar. 
Das Laden der Index-Datei dauert zu lange; 
das Skript bremst den Browser aus. 
Daher soll die Suche jetzt mit solr auf dem Server durchgeführt werden,
die Darstellung der Ergebnisse geschieht mit JavaScript.

## Arbeiten in Hugo

### Erstellung eines Suchindex

Mit der Template-Datei `index.json` produziert Hugo eine Index-Datei.
Im Unterschied zu der Ur-Version werden jetzt alle Felder außer legacy_id abgebildet,
außerdem auch die Kommentare.
Auf den Einsatz von `html-escape` wird verzichtet, um auch '&' korrekt darstellen zu können.

### Menüpunkt "Suche"

Es gibt unter `content\` einen neuen Menüpunkt "Suche", der zu der Suchseite führt.
In dem Template `search/list.html` wird ein Suchbutton mit dem Skript `solrsearch` verbunden.

### JavaScript

In der ersten Ausbaustufe fragt das Skript `solrsearch.js` das Eingabefeld ab.
Aus dem Wert wird der Abfrage-Befehl gebaut.
Der Server, der das Skript ausliefert, ist der, von dem die Seite kommt
(Abfrage mit `window.location.hostname`).
Der Port steht allerdings hardcodiert im Skript und muss ggf. angepasst werden.

Abgefragt werden ein generisches Feld (`_text_`) und der Titel (`title`). 
Der Titel wird doppelt gewichtet, daher erscheinen Seiten, die den gesuchten Begriff im Titel haben,
mit höherer Priorität und weiter vorne.

Bei Firefox muss ggf. die "Same Origin Policy" außer Betrieb gesetzt werden, 
z.B. mit einem geeigneten Plugin. 

## Solr

### Installation auf Testsystem

Die Installation auf einem Testsystem erfolgt wie hier beschrieben:
https://lucene.apache.org/solr/guide/8_7/solr-tutorial.html

* Start: `./bin/solr start`
* Einrichtung einer Collection: `./bin/solr create -c website`

Solr legt dann ein Schema unter `./server/solr/website` an.
Wir betreiben Solr im Standalone-Betrieb, nicht als Cloud.
In dem Unterverzeichnis `conf` liegt eine Datei `managed-schema`. 
Diese enthält allgemeine Felder. Wir tauschen sie durch die gleichnamige Datei
aus diesem Repo aus, die schon Anpassungen an unsere Datenstruktur enthält.


### Eigenes Schema 

`managed-schema` basiert auf den Default-Einstellungen und dem,
was Solr beim ersten Indizieren unserer Daten herausgefunden hat.
Das Schema kann über das Admin-UI (http://localhost:8983/solr/#/website/schema) 
bearbeitet werden.
Alle Felder, die durchsucht werden sollen, werden in das Copy-Field `_text_` kopiert.
Zur Anpassung können über das Admin-UI auch Felder gelöscht und wieder neu angelegt werden.
Wenn das Feld schon mit einem Copy-Field verbunden ist, muss diese Verbindung zuerst gelöscht werden,
bevor das Feld selbst gelöscht werden kann.

Die beiden wichtigen Felder `title` und `_text_` sind vom Typ `text_de`.
Dadurch können verschiedene Einstellungen für Deutsch genutzt werden.
Gegenüber der Voreinstellung sind die beiden Verarbeitungsschritte 
`Stem` und `Normalization` für Deutsch entfernt. 
Damit wird eine Unterscheidung von "Schranke" und "Schränke" möglich.

### Indizierung

Nach der Anpassung des Schemas können die Daten indiziert werden:

* `./bin/post -c website <pfadzu>/index.json`

Nach jeder Änderung des Schemas und bei einer Änderung der Daten werden diese zunächst gelöscht:

* `./bin/post -c website -d "<delete><query>*:*</query></delete>`

Ggf ist ein Stop und Neustart von Solr hilfreich.
Nach diesen Schritte ist eine Suche mit Solr möglich.
