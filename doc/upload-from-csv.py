#!/usr/bin/env/ python3
# Für den Upload von Bildern.
# Erstellt Galerie.
# Erstellt md-Files, kopiert Bilder an die richtige Stelle 
#
#
# Start im Repository-Hauptverzeichnis, also z.B. /website-hugo-poc
# Aufruf-Parameter: Quell-Verzeichnis Ziel-Album Id des letzen Bildes

import sys
import os
import subprocess
import datetime
import shutil
from slugify import slugify
import csv
import re
from time import sleep
import operator

def markdownify(text):
    newtext = re.sub("\[br\]","\n\n",text)
    newtext = re.sub("(\[b\]\s*)(.*?)(\s*\[/b\])","**\g<2>**",newtext,flags=re.IGNORECASE)
    newtext = re.sub(r"(\[url\])(.*)(\[/url\])","<\g<2>>",newtext,flags=re.IGNORECASE)
    newtext = re.sub(r"\[url=(.*)\](.*)\[/url\]","[\g<2>](\g<1>)",newtext,flags=re.IGNORECASE)
    return newtext


def buildcategoryheader(parent_path, cat_name, cat_description):
    catdirectory = parent_path + slugify(cat_name,max_length=40,word_boundary=True,stopwords=stopwordlist) +'/'
    overviewname = catdirectory +'gallery-index.md'
    subprocess.Popen([hugoexe,'new','-k', 'image-collection', overviewname]).wait()
    filename = catdirectory +'_index.md'
    subprocess.Popen([hugoexe,'new','-k', 'overview', filename]).wait()
    
    mdfilename = hugocontent + filename
    fin = open(mdfilename, "rt")
    data = fin.read()
    fin.close()

    newtitle = cat_name.replace('"',"'").strip()
    data = data.replace("TITLE", '"'+newtitle+'"')
    
    fin = open(mdfilename, "wt")
    fin.write(data)
    fin.close()
    
    #Content gibt es auch noch
    if cat_description:
        fin = open(mdfilename, "a")
        fin.write(markdownify(cat_description))
        fin.close()
    
    return catdirectory

# r ist die aktuelle Zeile im Resultset, catdirectory das Verzeichnis der image-collection    
def buildimage (catdirectory, row, current_id):
    # Hugo index.md in passendem Unterverzeichnis image_id anlegen lassen
    image_id = str(current_id)
    imagedirectory = catdirectory + image_id +'/'
    filename = imagedirectory + 'index.md'
    subprocess.Popen([hugoexe,'new','-k','image', filename]).wait()
    sleep(1)
    # index.md bearbeiten
    mdfilename = hugocontent + filename

    #Jetzt müssen in dem MD-File noch die richtigen Frontmatterparameter gesetzt werden
    fin = open(mdfilename, "rt")
    data = fin.read()
    fin.close()

    newtitle = row['Titel'].replace('"',"'")
    data = data.replace("TITLE TODO", newtitle)
    data = data.replace("PICTURE TODO", row['picture'])
    data = data.replace("WEIGHT OPTIONAL", str(row['weight']))
    if row['Konstrukteure']: 
        data = data.replace("KONSTRUKTEUR OPTIONAL", row['Konstrukteure'])
    else:
    #für fehlende Angaben gibt es Default-Werte
        data = data.replace("KONSTRUKTEUR OPTIONAL", "-?-")
    if row['Fotografen']: 
        data = data.replace("FOTOGRAPH OPTIONAL", row['Fotografen'])
    else:
    #für fehlende Angaben gibt es Default-Werte
        data = data.replace("FOTOGRAPH OPTIONAL", "-?-")
    try: 
        if row['keywords']:
            keywords = row['keywords'].split()
            keywordstring = '["' + '", "'.join(keywords) + '"]'
            data = data.replace('"KEYWORDS OPTIONAL"',keywordstring)
        # bei Bildern ohne Schlagworte soll die Zeile nicht in der Datei stehen    
        else:
            data = re.sub('schlagworte.*\n','',data)
    # wenn die Spalte fehlt
    except KeyError:
        print("Gibt keine Keywords")
        # die entsprechende Zeile soll dann auch nicht in der Datei stehen.
        data = re.sub('schlagworte.*\n','',data)
        
    data = data.replace("UPLOAD", "Website-Team")
    
    fin = open(mdfilename, "wt")
    fin.write(data)
    fin.close()

    #Content gibt es auch noch    
    fin = open(mdfilename, "a")
    fin.write(markdownify(row['Beschreibung']))
    fin.close()

    # Bild rüberkopieren
    # ggf. Bild von Imagemagick drehen lassen
    # mit Imagemagick passenden Thumbnail erzeugen
    imgdir = hugocontent+imagedirectory
    shutil.copy(upload_source + row['picture'], imgdir)
    subprocess.Popen(['mogrify','-auto-orient', imgdir+row['picture']]).wait()
    # hier jetzt eine Änderung bei den Thumbnails
    subprocess.Popen(['convert', imgdir+row['picture'],'-resize','191x191^','-gravity','Center','-crop','191x191+0+0','+repage',imgdir +'thumbnail.jpg']).wait()
#end buildimage

#parent_path ist der Pfad bis zur Überkategorie der aktuell anzulegenden Kategorie , endet mit /
def buildcategory(parent_path, current_max_id):
    new_id = current_max_id
    cat_name = open(upload_source + 'Titel').read()
    cat_description = open(upload_source + 'Beschreibung').read()
    
    catdirectory = buildcategoryheader(parent_path, cat_name, cat_description)

    with open(upload_source+csv_file_name, newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        sortierterreader = sorted(reader, key=lambda row: row['weight'], reverse=True)
        for row in sortierterreader:
            buildimage(catdirectory,row, new_id)
            new_id = new_id + 1

        
#Ende buildcategory

max_id = int(sys.argv[3])
upload_source = sys.argv[1]
csv_file_name = 'liste.csv'
hugoexe='/home/emi/bin/hugo_0.66.0_Linux-64bit/hugo'
hugocontent='./content/'
stopwordlist=["und","mit","oder","auch","der","die","das","auf","zur","von","dem", "fur","des", "in", "furs","aus","den","eine","einen"]

parent_directory = sys.argv[2]

buildcategory(parent_directory, max_id + 1)

