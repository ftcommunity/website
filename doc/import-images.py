#!/usr/bin/env/ python3
# Für den automatischen Import des Bilderpools aus der Datenbank auf ftcommunity.de.
# Soll Markdown-Files erstellen
# und die Dateien an die richtige Stelle kopieren.
# Die Verzeichnisnamen müssen nach Bedarf angepasst werden.
# Die Zugangsdaten für die Datenbank sind aus Sicherheitsgründen gelöscht.
#
#
# Start im Repository-Hauptverzeichnis, also z.B. /website-hugo-poc

import traceback
import pymysql.cursors
import sys
import os
import subprocess
import re
import datetime
import shutil
from slugify import slugify

def buildcategoryheader(parent_path, cat_id, cat_name, cat_description, cattype):
        catdirectory = parent_path + slugify(cat_name,max_length=40,word_boundary=True,stopwords=stopwordlist) +'/'
        overviewname = catdirectory +'gallery-index.md'
        subprocess.Popen([hugoexe,'new','-k', 'image-collection', overviewname]).wait()
        filename = catdirectory +'_index.md'
        subprocess.Popen([hugoexe,'new','-k', cattype, filename]).wait()
        
        mdfilename = hugocontent + filename
        fin = open(mdfilename, "rt")
        data = fin.read()
        fin.close()

        newtitle = cat_name.replace('"',"'")
        data = data.replace("TITLE", '"'+newtitle+'"')
        if cat_id:
            data = data.replace("LECACY OPTIONAL", 'categories/'+str(cat_id))
        
        fin = open(mdfilename, "wt")
        fin.write(data)
        fin.close()
        
        #Content gibt es auch noch
        if cat_id or cat_description:
            fin = open(mdfilename, "a")
            if cat_id:
                fin.write('<!-- https://www.ftcommunity.de/categories.php?cat_id=' +str(cat_id) + ' --> \n')
            fin.write(cat_description)
            fin.close()
        
        return catdirectory

# r ist die aktuelle Zeile im Resultset, catdirectory das Verzeichnis der image-collection    
def buildimage (catdirectory, r, weight):
    # Hugo index.md in passendem Unterverzeichnis image_id anlegen lassen
    image_id = str(r['image_id'])
    imagedirectory = catdirectory + image_id +'/'
    filename = imagedirectory + 'index.md'
    subprocess.Popen([hugoexe,'new','-k','image', filename]).wait()
    # index.md bearbeiten
    mdfilename = hugocontent + filename

    #Jetzt müssen in dem MD-File noch die richtigen Frontmatterparameter gesetzt werden
    fin = open(mdfilename, "rt")
    data = fin.read()
    fin.close()

    newtitle = r['image_name'].replace('"',"'")
    data = data.replace("TITLE TODO", newtitle )
    zeit = datetime.datetime.fromtimestamp(r['image_date']).strftime('%Y-%m-%dT%H:%M:%S')
    data = re.sub(r'date: .*','date: "'+ zeit+'"', data)
    data = data.replace("PICTURE TODO", r['image_media_file'])
    data = data.replace("WEIGHT OPTIONAL", str(weight))
    if r['image_konstrukteur']: 
        data = data.replace("KONSTRUKTEUR OPTIONAL", r['image_konstrukteur'])
    else:
    #für fehlende Angaben gibt es Default-Werte
        data = data.replace("KONSTRUKTEUR OPTIONAL", "-?-")
    if r['image_fotograf']: 
        data = data.replace("FOTOGRAPH OPTIONAL", r['image_fotograf'])
    else:
    #für fehlende Angaben gibt es Default-Werte
        data = data.replace("FOTOGRAPH OPTIONAL", "-?-")
    if r['image_keywords']:
        keywords = r['image_keywords'].split()
        keywordstring = '["' + '", "'.join(keywords) + '"]'
        data = data.replace('"KEYWORDS OPTIONAL"',keywordstring)
    data = data.replace("IMAGE_ID", str(r['image_id']))
    data = data.replace("CAT_ID", str(r['cat_id']))
    data = data.replace("USER_ID", str(r['user_id']))
    data = data.replace("IMAGE_DATE", str(r['image_date']))
    data = data.replace("IMAGE_ORDER", str(r['image_order']))
    #mit user_id Namen für Upload by raussuchen
    sql = 'select user_name from 4images_users where user_id=' + str(r['user_id'])
    cur.execute(sql)
    userresult = cur.fetchone()
    if userresult:
        data = data.replace("UPLOAD", userresult['user_name'])
    else:
    #für fehlende Angaben gibt es Default-Werte
        data = data.replace("UPLOAD", "-?-")
    data = data.replace("LECACY OPTIONAL", 'details/'+ str(image_id))
                
    fin = open(mdfilename, "wt")
    fin.write(data)
    fin.close()

    #Content gibt es auch noch    
    fin = open(mdfilename, "a")
    fin.write('<!-- https://www.ftcommunity.de/details.php?image_id='+str(image_id)+ ' -->\n')
    fin.write(r['image_description'])
    fin.close()

    # Bild rüberkopieren, Unterverzeichnis ist cat_id
    # mit Imagemagick passenden Thumbnail erzeugen
    imgdir = hugocontent+imagedirectory
    shutil.copy(backup+str(r['cat_id'])+'/'+r['image_media_file'], imgdir)
    subprocess.Popen(['convert', imgdir+r['image_media_file'],'-resize','382x382','-gravity','Center','-crop','191x191+0+0','+repage',imgdir +'thumbnail.jpg']).wait()
    #Kommentare suchen
    sql = 'select * from 4images_comments where image_id=' + image_id
    cur.execute(sql)
    commentresult = cur.fetchall()
    for cr in commentresult:
        commentname = imagedirectory + str(cr['comment_id']) + '.md'
        subprocess.Popen([hugoexe,'new','-k','comment', commentname]).wait()
        commentfilename = hugocontent + commentname
        fin = open(commentfilename, "rt")
        data = fin.read()
        fin.close()
        
        data = data.replace("TITLE", str(cr['comment_id']))
        zeit = datetime.datetime.fromtimestamp(cr['comment_date']).strftime('%Y-%m-%dT%H:%M:%S')
        data = re.sub(r'date: .*','date: "'+ zeit+'"', data)
        data = data.replace("UPLOAD", cr['user_name'])
        
        fin = open(commentfilename, "wt")
        fin.write(data)
        fin.close()

        #Content gibt es auch noch    
        fin = open(commentfilename, "a")
        fin.write(cr['comment_headline'] + cr['comment_text'])
        fin.close()
#end buildimage

#cat_id ist die aktuell anzulegende Kategorie, parent_path ist der Pfad bis zur ihrer Überkategorie, endet mit /
def buildcategory(parent_path, cat_id):
    sql = 'select * from 4images_categories where cat_id =' + str(cat_id)
    cur.execute(sql)
    catresult = cur.fetchone()
    cat_name = catresult['cat_name']
    cat_description = catresult['cat_description']
    
    # Gibt es Unterkategorien ?
    sql = 'select * from 4images_categories where cat_parent_id =' + str(cat_id)
    cur.execute(sql)
    result = cur.fetchall()
    # wenn es Unterkategorien gibt, Verzeichnis und _index.md anlegen
    if result:
        catdirectory = buildcategoryheader(parent_path, cat_id, cat_name, cat_description, 'overview')
        # in Unterkategorien verzweigen 
        for sub in result:
            sub_id = str(sub['cat_id'])
            buildcategory (catdirectory, sub_id)
        # gibt es vielleicht außerdem Bilder direkt in der Kategorie?
        sql = 'select * from 4images_images where cat_id=' + str(cat_id) + ' order by date(from_unixtime(image_date)) desc, image_order, image_id'
        cur.execute(sql)
        resultimages = cur.fetchall()
        weight = 1
        if resultimages:
            for ri in resultimages:
                buildimage(catdirectory,ri, weight)
                weight = weight + 1
    # wenn es keine Unterkategorien gibt, gibt es vielleicht Bilder?
    # das könnte man mit dem vorigen Fall zusammenlegen, ist aber jetzt auch egal
    else: 
        sql = 'select * from 4images_images where cat_id=' + str(cat_id) + ' order by date(from_unixtime(image_date)) desc, image_order, image_id'
        cur.execute(sql)
        resultimages = cur.fetchall()
        if resultimages:
            #Verzeichnis und _index.md für Kategorie anlegen, wenn Kategorie Bilder enthält
            imagedirectory = buildcategoryheader(parent_path, cat_id, cat_name, cat_description, 'overview')
            weight = 1
            for ri in resultimages:
                buildimage(imagedirectory,ri, weight)
                weight = weight + 1
    # wenn die Kategorie ganz leer ist, wird sie nicht angelegt            
#Ende buildcategory


hugoexe=''
# Es wird angenommen, dass die Files in einem lokalen Backup liegen
backup=''
hugocontent=''
stopwordlist=["und","mit","oder","auch","der","die","das","auf","zur","von","dem", "fur","des", "in", "furs","aus","den","eine","einen"]

try:
    conn = pymysql.connect(host='',
                           user='',
                           password='',
                           db='',
                           charset='utf8mb4',
                           cursorclass=\
                               pymysql.cursors.DictCursor)
    with conn.cursor() as cur:
        buildcategory('bilderpool/', 10)
       
except BaseException as ex:
    print('Fehler:', ex)
    print(traceback.format_exc())
finally:
    conn.close()
